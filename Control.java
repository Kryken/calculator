package com.hillel;

import java.io.IOException;

public interface Control {
    public double memoryRead() throws IOException;
    public double memoryPlus(double value) throws IOException;
    public double memoryMinus(double value) throws IOException;
    public void memoryClear() throws IOException;
}
