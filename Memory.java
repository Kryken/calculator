package com.hillel;

import java.io.IOException;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.nio.file.Paths;

public class Memory implements Control {
    String fileName = "log";
    public Memory() throws IOException {
        record("0");
    }

    private void record(String value) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(fileName);
        outputStream.write(value.getBytes());
        outputStream.flush();
        outputStream.close();
    }

    private String read() throws IOException {
        Scanner scanner = new Scanner(Paths.get(fileName), StandardCharsets.UTF_8.name());
        String data = scanner.next();
        scanner.close();
        return data;
    }

    public double memoryRead() throws IOException {
        System.out.println(read());
        double numFromMemory = Double.parseDouble(read());
        return numFromMemory;
    }

    public double memoryPlus(double value) throws IOException {
        double numFromMemory = Double.parseDouble(read());
        double res = value + numFromMemory;
        String str = Double.toString(res);
        record(str);
        return res;
    }

    public double memoryMinus(double value) throws IOException {
        double numFromMemory = Double.parseDouble(read());
        double res = numFromMemory - value;
        String str = Double.toString(res);
        record(str);
        return res;
    }
    public void memoryClear() throws IOException {
        record("0");
    }
}
