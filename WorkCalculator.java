package com.hillel;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorkCalculator {
private double num_1 = 0, num_2 = 0, res = 0;
private boolean isNum = false;
private String string_num = "";
private String calculatorOp;

    public String checkNum(String numOp) {
        String match = "";
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(numOp);
        while (matcher.find()) {
            match = matcher.group();
            isNum = parser();
        }
        return match;
    }

    public boolean parser() {return true;}
    public String getOp() {
        Scanner scanner = new Scanner(System.in);
        String op = scanner.next();
        return op;
    }

    public void start() throws IOException {
        Control control = new Memory();
        System.out.println("If you want to start the program enter 'start'.");
        String op = "";
        for (int i = 0; !op.equals("exit"); ++i) {
            op = getOp();
            string_num = checkNum(op);
            if (op.equals("start")) {
                System.out.println("You can use operators: '+', '-', '*', '/', '=', 'MR', 'M+', 'M-', 'MC', 'C', 'exit'.");
                System.out.println("Usage example: num --> operator '+', '-', '*', '/', 'M+', 'M-' --> num --> operator '='.");
                System.out.println("If you need information on operators, enter 'help'.");
            }
            else if (op.equals("help")) {
                System.out.println("'+' - add.");
                System.out.println("'-' - subtract.");
                System.out.println("'*' - multiply.");
                System.out.println("'/' - divide.");
                System.out.println("'=' - get result.");
                System.out.println("'MR' - memory read.");
                System.out.println("'M+' - add your number to the number from memory.");
                System.out.println("'M-' - subtract your number from the number in memory.");
                System.out.println("'MC' - clear memory.");
                System.out.println("'C' - clear calculator.");
                System.out.println("'exit' - close the program.");
            }
            else if (isNum) {
                if (num_1 != 0) {
                    num_2 = Double.parseDouble(string_num);
                    isNum = false;
                }
                else {
                    num_1 = Double.parseDouble(string_num);
                    isNum = false;
                }
            }
            else if (op.equals("+")) {
                calculatorOp = "+";
            }
            else if (op.equals("-")) {
                calculatorOp = "-";
            }
            else if (op.equals("*")) {
                calculatorOp = "*";
            }
            else if (op.equals("/")) {
                calculatorOp = "/";
            }
            else if (op.equals("=")) {
                res = Calculator.mathematicalOperations(num_1, calculatorOp, num_2);
                num_1 = res;
            }
            else if (op.equals("MR")) {
                num_1 = control.memoryRead();
            }
            else if (op.equals("M+")) {
                control.memoryPlus(num_1);
            }
            else if (op.equals("M-")) {
                control.memoryMinus(num_1);
            }
            else if (op.equals("MC")) {
                control.memoryClear();
            }
            else if (op.equals("C")) {
                num_1 = 0;
                num_2 = 0;
                calculatorOp = "";
            }
            else {
               if (!op.equals("exit")) {
                   System.out.println("Unknown operator. Enter 'help' to get information about operators.");
               }
               else {
                   System.out.println("Have a nice day! :)");
               }
            }
        }
    }
}
