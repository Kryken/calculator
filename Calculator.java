package com.hillel;

import java.io.IOException;
import java.util.Scanner;

public class Calculator {
    static int count = 0;

    public static double getValue() throws IOException {
        ++count;
        double value = 0.0;
        System.out.println("Enter num " + ": ");
        Scanner getNum = new Scanner(System.in);
        if (getNum.hasNextDouble()) {
            value = getNum.nextDouble();
            if (count == 2) {
                count = 0;
            }
            return value;
        } else {
            System.out.println("Your number is not correct, try again!");
            count = 0;
            start();
        }
        return value;
    }

    public static double add(double num1, double num2) {
        System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
        return num1 + num2;
    }

    public static double subtract(double num1, double num2) {
        System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
        return num1 - num2;
    }

    public static double multiply(double num1, double num2) {
        System.out.println(num1 + " * " + num2 + " = " + (num1 * num2));
        return num1 * num2;
    }

    public static double divide(double num1, double num2) {
        System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));
        return num1 / num2;
    }

    public static String getOperator() {

        Scanner getOp = new Scanner(System.in);
        String op = getOp.next();
        if (!op.equals("+") && !op.equals("-") && !op.equals("/") &&
                !op.equals("*") && !op.equals("MR") && !op.equals("M+") &&
                !op.equals("M-") && !op.equals("help")) {
            System.out.println("Operator error. Try again!");
            getOperator();
        }
        return op;
    }

    public static double mathematicalOperations(double num_1, String op, double num_2) throws IOException {
        double res = 0;
        if (op.equals("+")) {
            res = add(num_1, num_2);
        }
        else if (op.equals("-")) {
            res = subtract(num_1, num_2);
        }
        else if (op.equals("*")) {
            res = multiply(num_1, num_2);
        }
        else if (op.equals("/")) {
            if (num_1 == 0) {
                System.out.println("You can't divide by zero. Try again!");
                count = 0;
                start();
            }
            else {
                if (num_2 == 0) {
                    System.out.println("You can't divide by zero. Try again!");
                    start();
                } else {
                    res = divide(num_1, num_2);
                }
            }
        }
        else {
            System.out.println("Unknown operation. Enter 'help' to get information about operators.");
        }
        return res;
    }

    public static void start() throws IOException {
        mathematicalOperations(getValue(), getOperator(), getValue());
    }
}