package com.hillel;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        WorkCalculator workCalculator = new WorkCalculator();
        workCalculator.start();
    }
}
